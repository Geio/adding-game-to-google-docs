import urllib, json

import Sheets as sheet

###
#type == 1: "Activation Link"
#type == 2: "Steam Key"
#type == 3: "Steam Gift"
#type == 4: "Origin key"
#else: "Activation Link(Steam) / Steam Key"
###

# read data about games in steam
with urllib.request.urlopen("http://api.steampowered.com/ISteamApps/GetAppList/v0002/") as url:
    data = json.loads(url.read().decode())

#read data from sheet
values = sheet.read_data_from_sheet()

print(len(values))

# games to add to sheet
games = ["The Dwarves-23", "Hard Reset Redux-23", "Cities: Skylines-23", "METAL GEAR SOLID V: THE PHANTOM PAIN-23",
         "METAL GEAR SOLID V: GROUND ZEROES-23"]

for game in games:
    print(game)
    sheet.add_to_sheet(game.split("-")[0], game.split("-")[1], data, values)

#write to file
sheet.write_to_file_bullet(values)