# võta mängu info - n https://store.steampowered.com/api/appdetails?appids=552500
import urllib.request, json, re

def get_info_by_id(appid):
    with urllib.request.urlopen("http://store.steampowered.com/api/appdetails?appids=" + str(appid)) as url:
        data = json.loads(url.read().decode())
        return data[str(appid)]['data']

def get_id_by_name(gameName, data):
    for app in data['applist']['apps']:
        if app['name'].lower() == gameName.lower().replace('®', '').replace('™', ''):
            return app['appid']

#def get_info_by_ids(gameIDs):
#    for appid in gameIDs:
#        with urllib.request.urlopen("http://store.steampowered.com/api/appdetails?appids=" + str(appid)) as url:
#            data = json.loads(url.read().decode())
#            print(data[str(appid)]['data']['name'])

#def get_info_by_names(gameNames):
#    with urllib.request.urlopen("http://api.steampowered.com/ISteamApps/GetAppList/v0002/") as url:
#        data = json.loads(url.read().decode())
#        for gameName in gameNames:
#            for app in data['applist']['apps']:
#                if app['name'].lower() == gameName.lower().replace('®', '').replace('™', ''):
#                    print(app['appid'])
#                    print(type(get_header_image(get_info_by_id(app['appid']))))

def get_header_image(data):
    return data['header_image']

def get_name(data):
    return data['name']

def get_desc(data):
    info = data['detailed_description']
    new = re.sub('<.*?>', '\n', info)
    return new

def get_release_date(data):
    return data['release_date']['date']