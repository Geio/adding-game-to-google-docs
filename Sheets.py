from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
import steamGame as game

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'

store = file.Storage('token.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
    creds = tools.run_flow(flow, store)
service = build('sheets', 'v4', http=creds.authorize(Http()))

# Call the Sheets API
SPREADSHEET_ID = '1YSY5QjBA90pmQi8LIbn-zDs9YI3Kbog4rTvYxnS0MKo' # testing sheet
RANGE_NAME = 'A3:E'

def read_data_from_sheet():
    global SPREADSHEET_ID, RANGE_NAME
    result = service.spreadsheets().values().get(spreadsheetId=SPREADSHEET_ID,
                                                 range=RANGE_NAME,
                                                 valueRenderOption='FORMULA').execute()
    return result.get('values', [])

def write_to_file_bullet(sheetData):
    with open("C:/users/geio/desktop/mängudBullet.txt", "w") as f:
        for game in sheetData:
            f.write("* [" + str(game[2]) + "](" + game[1].split('"')[1] + ") - " + game[0] + "\n")


def add_to_sheet(gameInfo, type, gamesData, values):
    global SPREADSHEET_ID

    #placeholder values
    dataToInsert = [1,2,3,4]

    if type == 1:
        dataToInsert[0] = "Activation Link"
    elif type == 2:
        dataToInsert[0] = "Steam Key"
    elif type == 3:
        dataToInsert[0] = "Steam Gift"
    elif type == 4:
        dataToInsert[0] = "Origin key"
    else:
        dataToInsert[0] = "Activation Link(Steam) / Steam Key"

    if gameInfo.isdigit():
        #add hyperlink to steam game
        dataToInsert[1] = "=HYPERLINK(\"http://store.steampowered.com/app/" + str(gameInfo) + "/\" , IMAGE(\"" + game.get_header_image(game.get_info_by_id(gameInfo)) + "\",2))"
        #add game name
        dataToInsert[2] = game.get_name(game.get_info_by_id(gameInfo))

    else:
        dataToInsert[1] = "=HYPERLINK(\"http://store.steampowered.com/app/" + str(
            game.get_id_by_name(gameInfo, gamesData)) + "/\" , IMAGE(\"" + game.get_header_image(
            game.get_info_by_id(game.get_id_by_name(gameInfo, gamesData))) + "\",2))"
        dataToInsert[2] = gameInfo
    #extras
    dataToInsert[3] = "-"

    startIndex = 0
    endIndex = 1


    for i in range(len(values)):
        if str(values[i][2]).lower() == str(dataToInsert[2]).lower():
            print("MÄNG ON JUBA OLEMAS LISTIS!! - " + dataToInsert[2])
            return
        #compare game names by alphabetical order
        if str(values[i][2]).lower() > str(dataToInsert[2]).lower():
            startIndex = i+2
            endIndex = i+3
            break

        if i == len(values)-1:
            startIndex = len(values)+2
            endIndex = len(values)+3
            break


    # https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/batchUpdate et lisada uus rida4
    # https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/request#Request

    print(startIndex, endIndex)

    #add new empty row to sheet
    request = {
        'requests': [
            {
                'insertDimension': {
                    'range': {
                        'dimension': 'ROWS',
                        'startIndex': startIndex,
                        'endIndex': endIndex
                    }
                }
            }
        ]
    }
    insertRow = service.spreadsheets().batchUpdate(spreadsheetId=SPREADSHEET_ID, body=request)
    insertRow.execute()

    #insert info into row
    updateValues = {
        'responseValueRenderOption' : 'FORMULA',
        "valueInputOption": "USER_ENTERED",
        'data' : [{
            'range' : 'A' + str(endIndex) + ':E',
            'values' : [
                dataToInsert
            ]
        }]
    }

    # Write the results into spreadsheet
    toWrite = service.spreadsheets().values().batchUpdate(spreadsheetId=SPREADSHEET_ID, body=updateValues)

    toWrite.execute()


